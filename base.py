import torch
import torch.nn as nn
import torch.nn.functional as F


class MinMaxQuantizedConv2d(nn.Module):
    def __init__(self, conv_layer):
        super(MinMaxQuantizedConv2d, self).__init__()
        self.conv = conv_layer

    def min_max_quantize(self, tensor, bits=8):
        min_val = tensor.min()
        max_val = tensor.max()
        scale = (max_val - min_val) / (2 ** bits - 1)
        zero_point = torch.round(-min_val / scale).int()

        tensor_quantized = torch.clamp(torch.round(tensor / scale + zero_point), 0, 2 ** bits - 1).int()
        return tensor_quantized, scale, zero_point

    def forward(self, x):
        # 量化输入
        q_input, input_scale, input_zero_point = self.min_max_quantize(x)
        # 量化权重
        q_weight, weight_scale, weight_zero_point = self.min_max_quantize(self.conv.weight.data)

        # 反量化权重和输入
        q_weight = q_weight.float() * weight_scale + weight_zero_point.float()
        q_input = q_input.float() * input_scale + input_zero_point.float()

        # 使用量化后的权重进行卷积
        return self.conv._conv_forward(q_input, q_weight, self.conv.bias)


class MSEQuantizedConv2d(nn.Module):
    def __init__(self, conv_layer, bits=8):
        super(MSEQuantizedConv2d, self).__init__()
        self.conv = conv_layer
        self.bits = bits

    def mse_quantize(self, tensor):
        min_val = tensor.min()
        max_val = tensor.max()
        qmin = 0
        qmax = 2 ** self.bits - 1

        scale = (max_val - min_val) / (qmax - qmin)
        zero_point = qmin - min_val / scale

        best_mse = float('inf')
        best_scale = scale
        best_zero_point = zero_point

        for scale_adj in [0.95, 1.0, 1.05]:
            for zp_adj in [-10, 0, 10]:
                new_scale = scale * scale_adj
                new_zero_point = zero_point + zp_adj
                quantized = torch.clamp(torch.round(tensor / new_scale + new_zero_point), qmin, qmax)
                dequantized = quantized * new_scale + new_zero_point
                mse = F.mse_loss(dequantized, tensor)

                if mse < best_mse:
                    best_mse = mse
                    best_scale = new_scale
                    best_zero_point = new_zero_point

        return best_scale, best_zero_point

    def forward(self, x):
        scale_x, zero_point_x = self.mse_quantize(x)
        scale_w, zero_point_w = self.mse_quantize(self.conv.weight.data)

        x_quantized = torch.clamp(torch.round(x / scale_x + zero_point_x), 0, 2 ** self.bits - 1)
        w_quantized = torch.clamp(torch.round(self.conv.weight.data / scale_w + zero_point_w), 0, 2 ** self.bits - 1)

        x_dequantized = (x_quantized - zero_point_x) * scale_x
        w_dequantized = (w_quantized - zero_point_w) * scale_w

        return F.conv2d(x_dequantized, w_dequantized, self.conv.bias, stride=self.conv.stride,
                        padding=self.conv.padding, dilation=self.conv.dilation, groups=self.conv.groups)
