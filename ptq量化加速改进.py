import copy
import time

import torch
import torchvision.models as models
import torchvision.transforms as transforms
from torch.utils.data import DataLoader, Subset
from torchvision.datasets import CIFAR10
from torch.quantization import QuantStub, DeQuantStub, fuse_modules, prepare, convert
from torchvision.models import resnet18, ResNet18_Weights
from base import MinMaxQuantizedConv2d, MSEQuantizedConv2d

# 加载预训练的ResNet18模型
model = resnet18(weights=ResNet18_Weights.IMAGENET1K_V1)
model.fc = torch.nn.Linear(model.fc.in_features, 10)

# 保存原始模型的副本用于性能和精度评估
original_model = copy.deepcopy(model).eval()  # 注意这里使用 clone() 方法来创建原始模型的副本

# 替换模型中的部分层（这里以替换第一个卷积层为例）
# model.conv1 = torch.nn.Conv2d(3, 64, kernel_size=(7, 7), stride=(2, 2), padding=(3, 3), bias=False)
# model.conv1 = MinMaxQuantizedConv2d(model.conv1)
model.conv1 = MSEQuantizedConv2d(model.conv1)
# 调整模型的最后一个全连接层
# 将模型转换为评估模式
model.eval()


# 准备数据（这里以CIFAR-10为例）
transform = transforms.Compose([transforms.Resize(256),
                                transforms.CenterCrop(224),
                                transforms.ToTensor(),
                                transforms.Normalize(mean=[0.4914, 0.4822, 0.4465], std=[0.2023, 0.1994, 0.2010])
                                ])
dataset = CIFAR10(root='./data', train=True, download=True, transform=transform)
# 创建一个只包含前100个样本的子集
subset_dataset = Subset(dataset, list(range(100)))
# 使用子集数据来创建 DataLoader
data_loader = DataLoader(subset_dataset, batch_size=32, shuffle=True)

'''
# 使用库中自带的量化函数
# 定义量化和去量化的stubs
model.quant = QuantStub()
model.dequant = DeQuantStub()

# 指定要融合的层(可以提升量化的效率和效果)
fuse_modules(model, ['conv1', 'bn1', 'relu'], inplace=True)
# 准备模型进行量化
model = prepare(model)
# 使用数据集校准模型
with torch.no_grad():
    for images, _ in data_loader:
        model(images)


# 转换模型为量化版本
quantized_model = convert(model)

# 检查量化模型的效果
quantized_model.eval()
with torch.no_grad():
    for images, _ in data_loader:
        output = quantized_model(images)
        # print(output)
'''
# 函数用于评估模型的性能和精度
def evaluate_model(model, data_loader):
    model.eval()
    total_time = 0
    correct = 0
    total = 0
    with torch.no_grad():
        for images, labels in data_loader:
            start_time = time.time()
            outputs = model(images)
            end_time = time.time()
            total_time += (end_time - start_time)
            _, predicted = torch.max(outputs.data, 1)
            total += labels.size(0)
            correct += (predicted == labels).sum().item()
    accuracy = 100 * correct / total
    return total_time, accuracy


# 测量原始模型和量化模型的性能和精度
original_time, original_accuracy = evaluate_model(original_model, data_loader)
quantized_time, quantized_accuracy = evaluate_model(model, data_loader)

print(f"Original Model Time: {original_time:.3f} seconds")
print(f"Quantized Model Time: {quantized_time:.3f} seconds")
print(f"Speedup: {original_time / quantized_time:.2f}x")

print(f"Original Model Accuracy: {original_accuracy:.2f}%")
print(f"Quantized Model Accuracy: {quantized_accuracy:.2f}%")
print(f"Accuracy Drop: {original_accuracy - quantized_accuracy:.2f} percentage points")

# 如果需要，可以保存量化后的模型
torch.save(model.state_dict(), 'quantized_resnet18.pth')
